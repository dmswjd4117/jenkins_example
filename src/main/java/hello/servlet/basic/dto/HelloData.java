package hello.servlet.basic.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelloData {
    public Long id;
    public String title;
    public String body;
    public Long userId;
}
