package hello.servlet.basic.request;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hello.servlet.basic.dto.HelloData;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/spring")
public class SpringExample {
    Gson gson = new Gson();

    @GetMapping(value = "/hi")
    public ResponseEntity getSpring(HttpServletRequest request, HttpServletResponse
            response) {
        System.out.println("spring/hi");
        return ResponseEntity.ok("good good");
    }

    @Data
    static class CreateMemberResponse {
        public Long id;
        public CreateMemberResponse( Long id){
            this.id = id;
        }
    }
}

