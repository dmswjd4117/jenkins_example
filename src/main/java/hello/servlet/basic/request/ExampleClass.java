package hello.servlet.basic.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import hello.servlet.basic.dto.HelloData;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "responseJsonServlet", urlPatterns = "/posts")
public class ExampleClass extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse
            response)
            throws ServletException, IOException {
        //Content-Type: application/json
        response.setHeader("content-type", "application/json");
        response.setCharacterEncoding("utf-8");
        HelloData data = new HelloData();
        data.setId(1L);
        data.setTitle("kim");
        data.setUserId(20L);
        data.setBody("HELLO");

        HelloData data2 = new HelloData();
        data2.setId(2L);
        data2.setTitle("jim");
        data2.setUserId(22L);
        data2.setBody("kotlin");

        List<HelloData> list = new ArrayList<>();
        list.add(data);
        list.add(data2);

        String result = objectMapper.writeValueAsString(list);
        System.out.println(result);
        response.getWriter().write(result);
    }
}